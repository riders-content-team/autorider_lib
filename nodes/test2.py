#!/usr/bin/env python

from rosrider_lib.rosrider import ROSRider

robot = ROSRider('model_A',2)

while robot.is_ok():
    print("distance ", robot.front_laser_data())