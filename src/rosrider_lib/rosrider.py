#!/usr/bin/env python

import rospy
import math

from std_msgs.msg import String
from geometry_msgs.msg import Twist
from sensor_msgs.msg import Image
from std_srvs.srv import Empty
from nav_msgs.msg import Odometry
from std_srvs.srv import Trigger
from rosrider_description.srv import CameraSensor, RosriderControl, LaserSensor

from tf.transformations import quaternion_from_euler, euler_from_quaternion


class ROSRider:

    class _Image:
        def __init__(self):
            self.height = 0
            self.width = 0
            self.data = []

        def convert_str_to_rgb(self, data):
            self.data = []

            for i in range(self.height * self.width * 3):
                self.data.append(ord(data[i]))

    # init method. image_cb is camera callback function
    def __init__(self, robotName, index):
        self.max_laser_range = 10.0

        rospy.init_node("rosrider_node")

        self.image = self._Image()
        self.is_shutdown = False

        self._init_robot_control("/rosrider_control" + str(index))

        self._init_sensor_services(index)
        self._init_game_controller()

        self.linear_speed = 0
        self.angular_speed = 0
        self.step_size = 50

    def _init_game_controller(self):
        try:
            rospy.wait_for_service("/user_used_distance_sensor", 5.0)

            self.user_used_distance_sensor = rospy.ServiceProxy('user_used_distance_sensor', Trigger)

        except (rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("Service call failed: %s" % (e,))

    def _init_robot_control(self, controlName):
        try:
            rospy.wait_for_service(controlName, 1)

            self.control = rospy.ServiceProxy(controlName, RosriderControl)
            return True

        except (rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("Service call failed: %s" % (e,))
            print("No sensor")
            return False

    def _init_sensor_services (self, index):
        try:
        
            if index == 1:
                camera_string = "/front_camera_Player"
                laser_string = "/rosrider_Player"
            if index == 2:
                camera_string = "/front_camera_A"
                laser_string = "/rosrider_A"
            if index == 3:
                camera_string = "/front_camera_B"
                laser_string = "/rosrider_B"
            if index == 4:
                camera_string = "/front_camera_C"
                laser_string = "/rosrider_C"
            if index == 5:
                camera_string = "/front_camera_D"
                laser_string = "/rosrider_D"
          
            rospy.wait_for_service(camera_string + "/rosrider_get_image", 5.0)
            #rospy.wait_for_service(laser_string + "/get_front_range", 5.0)
            rospy.wait_for_service(laser_string + "/get_right_range", 5.0)
            rospy.wait_for_service(laser_string + "/get_left_range", 5.0)
            self.image_data_service = rospy.ServiceProxy(camera_string + "/rosrider_get_image", CameraSensor)
            #self.front_laser_service = rospy.ServiceProxy(laser_string + "/get_front_range", LaserSensor)
            self.right_laser_service = rospy.ServiceProxy(laser_string + "/get_right_range", LaserSensor)
            self.left_laser_service = rospy.ServiceProxy(laser_string + "/get_left_range", LaserSensor)

            resp = self.image_data_service()

            self.image.height = resp.height
            self.image.width = resp.width

            self.image.convert_str_to_rgb(resp.data)

            # Todo clean up duplicates perhaps in image
            # This is done like this to match conventions of drone race
            # and make it easier to read
            self.camera_height = resp.height
            self.camera_width = resp.width

            return True

        except (rospy.ServiceException, rospy.ROSException), e:
            rospy.logerr("Service call failed: %s" % (e,))

            return False

    # def front_laser_data(self):
    #    resp = self.front_laser_service()
    #    return resp.data

    def right_laser_data(self):
        self.user_used_distance_sensor()
        resp = self.right_laser_service()
        if math.isinf(resp.data):
            return self.max_laser_range
        return resp.data

    def left_laser_data(self):
        self.user_used_distance_sensor()
        resp = self.left_laser_service()
        if math.isinf(resp.data):
            return self.max_laser_range
        return resp.data

    @property
    def forward_right(self):
        return self.right_laser_data()

    @property
    def forward_left(self):
        return self.left_laser_data()

    def _check_game_controller(self):
        rospy.wait_for_message('/simulation_metrics', String, timeout=5)

    def move(self, linear_speed):
        self.linear_speed = linear_speed
        if self.control:
            self.control(self.linear_speed, self.angular_speed, self.step_size)
        else:
            return

    def rotate(self, angular_speed):
        self.angular_speed = angular_speed
        if self.control:
            self.control(self.linear_speed, self.angular_speed, self.step_size)
        else:
            return

    def get_camera_data(self):
        image = self.image_data()
        camera_data = []
        for i in range(image.width * image.height * 3):
            camera_data.append(ord(image.data[i]))
        return camera_data

    def image_data(self):
        resp = self.image_data_service()

        self.image.data = resp.data

        return self.image

    def execAI(self, speed, color):
        camera_data = self.get_camera_data()
        count_pixels = 0
        sum_i = 0
        for i in range(self.camera_width):
            a = i * 3
            r = camera_data[a+0]
            g = camera_data[a+1]
            b = camera_data[a+2]
            if color == 0:
                level = r - max(g, b) # drive on red
            if color == 1:
                level = g - max(b, r) # drive on green
            if color == 2:
                level = b - max(r, g) # drive on blue
            if level > 200:
                sum_i += i
                count_pixels += 1
        
        if count_pixels > 0:
            index = int(sum_i/count_pixels)
        else:
            index = self.camera_width/2

        # Follow the line
        error = self.camera_width / 2 - index
        gain = 0.01
        command = gain * error
        self.move(speed)
        self.rotate(command)

    def is_ok(self):
        if not rospy.is_shutdown():
            self.image_data()
            rospy.sleep(0.025)
            return True
        else:
            return False
